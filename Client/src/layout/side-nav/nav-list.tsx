import DashboardOutlined from '@mui/icons-material/DashboardOutlined';
import Forum from '@mui/icons-material/Forum';
import Analytics from '@mui/icons-material/Analytics';
import FolderOpen from '@mui/icons-material/FolderOpen';

const NavbarList = [
    {
        icon: DashboardOutlined,
        desc: 'Dashboard',
        secondDesc: '',
        url: '/home',
        badge: 0,
        subList: [],
    },
    {
        icon: FolderOpen,
        desc: 'Question',
        url: '/questions',
        secondDesc: '',
        badge: 0,
        subList: [],
    },
];

const NavbarListWithoutLoggedIn = [
    {
        icon: DashboardOutlined,
        desc: 'Home Page',
        secondDesc: '',
        url: '/home',
        badge: 0,
        subList: [],
    },
    {
        icon: FolderOpen,
        desc: 'Question',
        url: '/questions',
        secondDesc: '',
        badge: 0,
        subList: [],
    }
]

export {NavbarList, NavbarListWithoutLoggedIn} ;
