import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import {Avatar, Button, Stack} from "@mui/material";
import {useLocation, useNavigate} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {IRootState} from "../../redux/auth/state";
import {logout} from "../../redux/auth/action";
import {useEffect, useState} from "react";
import {fetchUserData} from "../../redux/UserData/thunks";
import '../../assets/scss/index.scss'

export default function MenuAppBar() {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const isLoggedIn = useSelector((state: IRootState) => state.auth.isLoggedIn)
    const [checkIsLogIn, setCheckIsLogIn] = useState(false)
    const [avatar, setAvatar] = useState("")
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {

        setAnchorEl(null);
    };

    const userProfile = () => {
        navigate("/user")
        setAnchorEl(null);
    }

    const fetchLoginStatus = async() => {
        let res = await dispatch(fetchUserData()).unwrap()
        console.log("what i want :::::::::::::: ", res.userInfo[0].avatar)
        setAvatar(res.userInfo[0].avatar)
        if(isLoggedIn === true ){
            setCheckIsLogIn(true)
        }
        setCheckIsLogIn(false)
    }

    const handleLogout = () => {
        if (isLoggedIn === true || isLoggedIn === undefined){
            dispatch(logout(false))
            navigate('/home')
            setAnchorEl(null);
        }

    }

    const login = () => {
        navigate("/login")
    }


    const navBtnChange = () => {
      if(isLoggedIn === false || isLoggedIn === undefined){
          return <Button variant="outlined" color="inherit" onClick={login}>Login</Button>
      }
      return <></>
    }

    useEffect(() => {
        fetchLoginStatus()
    },[])

    return (
        <AppBar position="static"
                sx={{backgroundColor: "#11101D", borderRadius: '0px 16px 16px 0px', borderRight: '0px'}}>
            <Toolbar sx={{display: "flex", justifyContent: "end",}}>
                <Stack spacing={2} direction="row">
                    {navBtnChange()}
                </Stack>

                {isLoggedIn && fetchLoginStatus ? (
                    <div>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <Avatar src={`http://localhost:8080/${avatar}`} alt="user"/>
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={userProfile}>Profile</MenuItem>
                            <MenuItem onClick={handleLogout}>Logout</MenuItem>
                        </Menu>
                    </div>
                ) : (
                    <></>
                )}
            </Toolbar>
        </AppBar>
    );
}
