import {useState} from "react";

export default function useQuestionDetails(){

    const [questionDetails, setQuestionDetails] = useState<any>(null)
    const fetchQuestionDetails = async (questionId:number)=>{
        if (!questionId){
            console.log("Invalid questionId")
            return
        }

        if (!localStorage.getItem("token")){
            console.log("Invalid token")
            return
        }
        let res = await fetch(`http://localhost:8080/question-details/${questionId}`,{
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        })
        let data = await res.json()
        console.log('data = ', data)
        if (res.ok){
            setQuestionDetails(data)
        }
    }

    return {fetchQuestionDetails, questionDetails, setQuestionDetails}
}