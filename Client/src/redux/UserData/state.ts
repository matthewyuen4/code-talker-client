import { IUserDataState } from "./reducer";

export interface IRootState {
  userData: IUserDataState
}