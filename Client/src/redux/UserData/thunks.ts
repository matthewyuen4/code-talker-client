import { createAsyncThunk } from "@reduxjs/toolkit";

const fetchUserData: any = createAsyncThunk('fetchUserData', async (params, { fulfillWithValue, rejectWithValue }) => {
    const res = await fetch('http://localhost:8080/userData', {
        method: 'GET',
        headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`,
            "Content-type": "application/json",
        },
        body: JSON.stringify(params)
    })
    const data = await res.json()
    console.log("data from thunks", data)

    if (res.ok) {
        return fulfillWithValue(data)
    }
    return rejectWithValue(data)
})

export { fetchUserData }