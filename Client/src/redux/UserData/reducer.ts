import { createReducer } from "@reduxjs/toolkit";
import { fetchUserData } from "./thunks";
import jwtDecode from "jwt-decode";

export interface IUserDataState {
    userData: {
        userInfo: Array<any>,
        favorites: Array<any>,
        history: Array<any>,
        date: Array<any>,
    },
}
const initialUserData: IUserDataState = {
    userData: {
        userInfo: [""],
        favorites: [""],
        history: [""],
        date: [""],
    },
}

const userDataReducer = createReducer(initialUserData, (build) => {
    build.addCase(fetchUserData.fulfilled, (state, action) => {
        state.userData = action.payload
    })
    build.addCase(fetchUserData.rejected, (state, action) => {
        console.log("userDataReducer failed")
    })
})

export default userDataReducer