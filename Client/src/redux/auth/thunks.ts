import { createAsyncThunk } from "@reduxjs/toolkit";

interface LoginForm {
    username: string
    password: string
}

interface LoginWithTokenForm {
    token: string
}

interface RegisterForm {
    username: string
    password: string
    email: string
    files: any
}


const login: any = createAsyncThunk('loginLocal', async (params: LoginForm, { fulfillWithValue, rejectWithValue }) => {
    try {
        const res = await fetch('http://localhost:8080/user/login', {
            method: 'POST',
            headers: {
                "Content-type": "application/json",
            },
            body: JSON.stringify({ ...params })
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }

})

const fbLogin: any = createAsyncThunk("fbThunk", async (params, thunkApi) => {
    const fbToken = params
    console.log(fbToken)
    const res = await fetch("http://localhost:3000/login/fb", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ token: fbToken })
    })
    const data = await res.json()
    if (res.ok) {
        const token = data.token
        return thunkApi.fulfillWithValue(token)
    }
    return thunkApi.rejectWithValue("Login Fail Trough FB")
})

const userRegister: any = createAsyncThunk('userRegister', async (formData: any, { fulfillWithValue, rejectWithValue }) => {
    try {
        const res = await fetch('http://localhost:8080/user/register', {
            method: 'POST',
            body: formData!,
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }
})

const loginWithToken: any = createAsyncThunk('loginLocal', async (params: LoginWithTokenForm, { fulfillWithValue, rejectWithValue }) => {
    try {
        if (params.token == null) {
            return rejectWithValue('not logged in');
        }
        const res = await fetch('http://localhost:8080/user/loginWithToken', {
            method: 'POST',
            headers: {
                "Content-type": "application/json",
            },
            body: JSON.stringify({ ...params })
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }
})

export { login, loginWithToken, fbLogin, userRegister }