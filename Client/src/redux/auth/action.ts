import {createAction} from "@reduxjs/toolkit"

const logout = createAction<boolean>('auth/logout')

export { logout }