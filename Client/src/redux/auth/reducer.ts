import { createReducer } from "@reduxjs/toolkit";
import { login, fbLogin, userRegister } from "./thunks";
import jwtDecode from "jwt-decode";
import {logout} from "./action";

export interface IAuthState {
    username: string
    isLoggedIn: boolean | undefined
    role: string
    avatar: string
}

export interface IncomingJWTPayloadState {
    user_id: number,
    username: string,
    role: string
    avatar: string
    status: string
}

const initialState: IAuthState = {
    username: "",
    role: "",
    avatar: "",
    isLoggedIn: undefined
}

const authReducer = createReducer(initialState, (build) => {
    build
        .addCase(login.fulfilled, (state, action) => {
            handleToken(state, action)
        })
        .addCase(fbLogin.fulfilled, (state, action) => {
            handleToken(state, action)
        })
        .addCase(userRegister.fulfilled, (state, action) => {
            handleToken(state, action)
        })
        .addCase(logout, (state, action) => {
            state.isLoggedIn = action.payload
            localStorage.removeItem("token")
        })
});

const handleToken = (state, action) => {
    const { token } = action.payload;
    localStorage.setItem("token", token)
    const jwtPayload: IncomingJWTPayloadState = jwtDecode(token)
    state.isLoggedIn = true
    state.role = jwtPayload.role
    state.status = jwtPayload.status
    state.username = jwtPayload.username;
}

export default authReducer