import {IAuthState} from "./reducer";

export interface IRootState {
  auth: IAuthState
}