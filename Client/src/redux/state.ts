import { IAuthState } from "./auth/reducer";
import { IRoomInformationState} from "./room/reducer";
import { IUserDataState } from "./UserData/reducer";

export interface IRootState {
    auth: IAuthState,
    roomInfo: IRoomInformationState,
    userData: IUserDataState,
}