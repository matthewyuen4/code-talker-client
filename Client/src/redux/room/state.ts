import { IRoomInformationState} from "./reducer";

export interface IRootState {
  roomInfo: IRoomInformationState[]
}