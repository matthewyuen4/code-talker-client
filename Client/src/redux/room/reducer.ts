import {createReducer} from "@reduxjs/toolkit";
import {createRoomAndJoinRoomViaURL} from "./thunks";


export interface IRoomInformationState {
    sentInvitationEmail: any
    roomId: string
    roomURL: string
    username: string
}

const initialState: IRoomInformationState= {
    sentInvitationEmail: [],
    roomId: "",
    roomURL: "",
    username: ""
}

const createRoomReducer = createReducer(initialState, (build) => {
    build
        .addCase(createRoomAndJoinRoomViaURL, (state, action) => {
            state.roomURL= action.payload
            state.username= action.payload
            state.roomId=action.payload
        })
});

export default createRoomReducer