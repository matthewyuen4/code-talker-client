import {createAsyncThunk} from "@reduxjs/toolkit";

interface QuestionsData{
    title: string
    programming_languages_id: string
    difficulties_id: string
    categories_id: string
    created_by_user_id: string
    question_md: string
    question: string
}

interface RoomId {
    roomId: string
}

const createRoomAndJoinRoomViaURL: any = createAsyncThunk('joinRoom', async (params: any, {fulfillWithValue, rejectWithValue}) => {
    try {
        const res = await fetch('http://localhost:8080/code-room', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body:JSON.stringify(params)
        })
        const data = await res.json()
        if(res.ok){
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    }catch (e) {
        return rejectWithValue(e)
    }
})

// const createRoomWithoutEmail: any = createAsyncThunk('justClickRoom', async (params: any, {fulfillWithValue, rejectWithValue}) => {
//     try {
//         const res = await fetch('http://localhost:8080/clicked-code-room', {
//             method: 'POST',
//             headers: {
//                 'Content-type': 'application/json',
//                 "Authorization": `Bearer ${localStorage.getItem("token")}`
//             },
//             body:JSON.stringify(params)
//         })
//         const data = await res.json()
//         if(res.ok){
//             return fulfillWithValue(data)
//         }
//         return rejectWithValue(data)
//     }catch (e) {
//         return rejectWithValue(e)
//     }
// })

const fetchUserRoomData: any = createAsyncThunk('findUser', async (params: any, {fulfillWithValue,rejectWithValue}) => {
    try {
        const res = await fetch('http://localhost:8080/getUserRoomFromDB', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body:JSON.stringify(params)
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    }catch (e) {
        return rejectWithValue(e)
    }
})

export { createRoomAndJoinRoomViaURL, fetchUserRoomData }