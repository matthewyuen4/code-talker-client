import { createAsyncThunk } from "@reduxjs/toolkit";

interface QuestionsData {
    title: string
    programming_languages_id: string
    difficulties_id: string
    categories_id: string
    created_by_user_id: string
    question_md: string
    question: string
}


const questions: any = createAsyncThunk('questionFetch', async (params: QuestionsData, { fulfillWithValue, rejectWithValue }) => {
    try {
        const res = await fetch('http://localhost:8080/question', {
            method: 'GET',
            headers: {
                "Content-type": "application/json"
            },
            // body: JSON.stringify()

        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }
})

const getQuestionPopulation: any = createAsyncThunk('index', async (params: any, { fulfillWithValue, rejectWithValue }) => {
    try {
        // console.log(params.selectedQuestion)
        const res = await fetch('http://localhost:8080/popular-question', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
            },
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }
})

const saveQuestionToFavorites: any = createAsyncThunk('addFavoritesList', async (params: any, { fulfillWithValue, rejectWithValue }) => {
    try {
        const res = await fetch('http://localhost:8080/addQuestionToFav', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify(params)
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }
})

const deleteQuestionToFavorites: any = createAsyncThunk('deleteFavoritesList', async (params: any, { fulfillWithValue, rejectWithValue }) => {
    try {
        const res = await fetch('http://localhost:8080/deleteQuestionToFav', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify(params)
        })
        const data = await res.json()
        if (res.ok) {
            return fulfillWithValue(data)
        }
        return rejectWithValue(data)
    } catch (e) {
        return rejectWithValue(e)
    }
})



export { questions, saveQuestionToFavorites, getQuestionPopulation, deleteQuestionToFavorites }