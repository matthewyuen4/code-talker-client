import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth/reducer";
import createRoomReducer from './room/reducer'
import userDataReducer from "./UserData/reducer";

const store = configureStore({
  reducer: {
    auth: authReducer,
    question: createRoomReducer,
    userData: userDataReducer,
  },
  devTools: true
})

export default store