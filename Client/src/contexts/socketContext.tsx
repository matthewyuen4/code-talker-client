import React, { createContext, useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { io } from "socket.io-client";
import { fetchUserRoomData } from "../redux/room/thunks";
import { useParams } from "react-router-dom";

export const SocketStore = createContext<any>(null);

export function SocketProvider(props: any) {
	const [sk, setSk] = useState<any>(null);
	const [content, setContent] = useState("console.log('hello world!');");
	const [answer, setAnswer] = useState<string[]>(["execution window"]);
	const dispatch = useDispatch();
	const { roomId } = useParams();
	const [player, setPlayer] = useState([]);
	const [mdQ, setMdQ] = useState("");
	const [qId, setQId] = useState(0);
	const [roomMeta, setRoomMeta] = useState({
		userAvatar: "A",
		roomId: roomId,
	});
	const [qTitle, setQTitle] = useState("");
	const [userId, setUserId] = useState([]);
	const [submitStatus, setSubmitStatus] = useState(false);

	const getRoomInfoFromDB = useCallback(async () => {
		let res = await dispatch(fetchUserRoomData({ roomId: roomId })).unwrap();
		console.log("editorRes:", res[0]);
		let test = res.map((userPlayer) => userPlayer.avatar);

		console.log("roomID avatar:", test);
		const questTitle = res[0].question_md.slice(0, -3);
		setPlayer(test);
		setMdQ(res[0].mdContent);
		setQTitle(questTitle);
		setQId(res[0].questions_id);
		setUserId(res.map((user) => user.user_id));
		return;
	}, [dispatch, roomId]);

	const initSocket = useCallback(async () => {
		console.log("init" + roomId);
		let roomInfo = await dispatch(fetchUserRoomData({ roomId: roomId })).unwrap();
		console.log("roomInfo: ", roomInfo);

		setRoomMeta({ userAvatar: roomInfo.avatar, roomId: roomId });
		setSk(io("192.168.59.109:8080", { transports: ["websocket"] }));
	}, [dispatch, roomId]);

	const handleEditorChange = (value: string) => {
		console.log("onchange triggered");
		sk.emit("CODE_CHANGED", { code: value, roomId: roomMeta.roomId });
		console.log(roomMeta);
	};
	type serverContent = {
		code: string;
		id: number;
	};
	useEffect(() => {
		if (!sk || roomMeta.roomId === "") {
			getRoomInfoFromDB();
			initSocket();
		} else {
			sk.on("connect", () => {
				sk.emit("CONNECTED_TO_ROOM", {
					roomId: roomMeta.roomId,
					socketId: sk.id,
					questionId: qId,
				});
				console.log("socket id", sk.id);
			});
			sk.on("code_current", (serverContent: serverContent) => {
				console.log(serverContent);

				if (serverContent.id === sk.id) {
					return;
				}
				setContent(serverContent.code);
			});
			sk.on("member_join", (username: any) => {
				console.log("member_join triggered: ", username);
			});
			sk.on("test_case", (data: any) => {
				console.log(data.result);
				data.status === "pass"
					? setAnswer(data.result.split("\n"))
					: setAnswer(data.result.split("\n").slice(0, 1));
			});
			sk.on("result", (data: any) => {
				console.log("submit_result triggered", data.result);
				if (data.status === "pass") {
					setAnswer(data.result.split("\n").slice(2, 5));
					setSubmitStatus(true);
				} else {
					setAnswer(data.result.split("\n").slice(3, 6));
				}
				// : setAnswer(data.result.split("\n").slice(3, 6));
			});
		}
	}, [initSocket, sk, content, roomMeta, getRoomInfoFromDB, mdQ]);
	const contextValue = {
		content,
		handleEditorChange,
		setContent,
		roomMeta,
		sk,
		setAnswer,
		answer,
		getRoomInfoFromDB,
		mdQ,
		qTitle,
		qId,
		userId,
		player,
		submitStatus,
	};
	return <SocketStore.Provider value={contextValue}>{props.children}</SocketStore.Provider>;
}
