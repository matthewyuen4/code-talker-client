export default function NotFoundError() {
  return(
    <div>
      <h1>404 Not Found</h1>
    </div>
  )
}