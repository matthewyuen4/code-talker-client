import * as React from 'react';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import {useEffect, useState} from "react";

const Img = styled('img')({
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
});

export default function ComplexGrid() {
    const [isClicked, setIsClicked] = useState(false)

    return (
        <Paper
            sx={{
                p: 2,
                margin: 'auto',
                maxWidth: 500,
                flexGrow: 1,
                // backgroundColor: (theme) =>
                //     theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
                backgroundColor: '#fff'
            }}
        >
            {}
            <Grid container spacing={2}>
                <Grid item>
                    <ButtonBase sx={{ width: 128, height: 128 }}>
                        <Img alt="complex" src="../assets/images/jsLogo.png" />
                    </ButtonBase>
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs>
                            <Typography gutterBottom variant="subtitle1" component="div">
                                Question Title
                            </Typography>
                            <Typography variant="body2" gutterBottom>
                                please answer one plus one equal ?
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                roomID: 1030114
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                host by David
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Typography variant="subtitle1" component="div" onClick={() => isClicked ? setIsClicked(false) : setIsClicked(true) }>
                            {isClicked ? <TurnedInIcon /> : <BookmarkBorderIcon/>}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    );
}
