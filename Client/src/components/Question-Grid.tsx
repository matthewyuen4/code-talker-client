import * as React from 'react';
import {DataGrid, gridClasses} from '@mui/x-data-grid';
import {useDemoData} from '@mui/x-data-grid-generator';
import {alpha, styled} from '@mui/material/styles';
import {useDispatch} from "react-redux";
import {questions} from "../redux/questions/thunks";
import {useEffect, useState} from "react";
import QuestionInfoModal from "./QuestionInfoModal";
import "../assets/scss/question-grid.scss"
import Paper from "@mui/material/Paper";
import Box from "@mui/material//Box"
import "../assets/scss/grid-bottom-style.scss"

const Item = styled(Paper)(({theme}) => ({
    backgroundColor: '#0000003b',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const VISIBLE_FIELDS = ['name', 'country', 'dateCreated', 'isAdmin'];
const ODD_OPACITY = 0.2;

export default function QuestionDataGrid() {

    const [selectedQuestion, setSelectedQuestion] = React.useState(null)
    const {data, loading} = useDemoData({
        dataSet: 'Employee',
        visibleFields: VISIBLE_FIELDS,
        rowLength: 100,
    });
    const [question, setQuestion] = useState([])
    const dispatch = useDispatch()


    const fetchQuestion = async () => {
        let res = await dispatch(questions()).unwrap()
        console.log(res)
        setQuestion(res)
    }

    useEffect(() => {
        if (!question || question.length === 0) {
            fetchQuestion()
        }
    }, [question, fetchQuestion])


    const StripedDataGrid = styled(DataGrid)(({theme}) => ({
        [`& .${gridClasses.row}.odd`]: {
            backgroundColor: "hsla(0,0%,100%,.07)",
            color: "white",
            '&:hover, &.Mui-hovered': {
                backgroundColor: alpha(theme.palette.primary.main, ODD_OPACITY),
                '@media (hover: none)': {
                    backgroundColor: 'transparent',
                },
            },
        },
        [`& .${gridClasses.row}.even`]: {
            backgroundColor: "#11101D",
            className: "addPaddingForGrid",
            color: "white",
            '&:hover, &.Mui-hovered': {
                backgroundColor: alpha(theme.palette.primary.main, ODD_OPACITY),
                '@media (hover: none)': {
                    backgroundColor: 'transparent',
                },
            },
            '&.Mui-selected': {
                backgroundColor: alpha(
                    theme.palette.primary.main,
                    ODD_OPACITY + theme.palette.action.selectedOpacity,
                ),
                '&:hover, &.Mui-hovered': {
                    backgroundColor: alpha(
                        theme.palette.primary.main,
                        ODD_OPACITY +
                        theme.palette.action.selectedOpacity +
                        theme.palette.action.hoverOpacity,
                    ),
                    // Reset on touch devices, it doesn't add specificity
                    '@media (hover: none)': {
                        backgroundColor: alpha(
                            theme.palette.primary.main,
                            ODD_OPACITY + theme.palette.action.selectedOpacity,
                        ),
                    },
                },
            },
        },
    }));


    return (
        <Box sx={{flexGrow: 1, py: 4, px: 4}}>
            <Item>
                <QuestionInfoModal setSelectedQuestion={setSelectedQuestion}
                                   selectedQuestion={selectedQuestion}></QuestionInfoModal>
                <div style={{minHeight: "40vh", height: "100vh", width: '100%', outline: "none", color: "whitesmoke"}}>
                    <StripedDataGrid
                        key='questionId'
                        sx={{border: "none", }}
                        loading={loading}
                        rows={question}
                        onCellClick={(cell) => {
                            if (cell.field === 'title') {
                                setSelectedQuestion(cell.row)
                            }
                        }}
                        columns={[
                            {
                                field: 'title',
                                headerName: 'Title',
                                type: 'string',
                                width: 400,
                                headerClassName: 'question-data-grid-header',
                            },
                            {
                                field: 'programming_language',
                                headerName: 'Language',
                                type: 'string',
                                width: 200,
                                headerClassName: 'question-data-grid-header',
                            },
                            {
                                field: 'difficulty',
                                headerName: 'Difficulty',
                                type: 'string',
                                width: 180,
                                headerClassName: 'question-data-grid-header',
                            },
                            {
                                field: 'category',
                                headerName: 'Category',
                                type: 'string',
                                width: 180,
                                headerClassName: 'question-data-grid-header',
                            },
                            {
                                field: 'username',
                                headerName: 'Created By',
                                type: 'string',
                                width: 200,
                                headerClassName: 'question-data-grid-header',
                            },
                        ]}
                        getRowClassName={(params) =>
                            params.indexRelativeToCurrentPage % 2 === 0 ? 'even' : 'odd'
                        }
                    />
                </div>
            </Item>
        </Box>
    );
}
