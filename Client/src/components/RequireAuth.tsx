import { useSelector } from "react-redux";
import { Outlet, Navigate } from "react-router-dom";
import { IRootState } from "../redux/auth/state";
import {useLocation} from "react-router-dom";
export default function RequireAuth() {
  const isLoggedIn = useSelector((state: IRootState) => state.auth.isLoggedIn);
  const location = useLocation();

  return (

    isLoggedIn || location.pathname === '/home' ? (
      <div>
        <Outlet />
      </div>
    ) : (
      <Navigate to={"/login"} />
    )
  )
}
