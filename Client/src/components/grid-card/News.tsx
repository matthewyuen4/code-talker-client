import { Box } from "@mui/material";
import React from "react";
import "../../assets/scss/news.modual.scss"

const slideWidth = 20;

const _items = [
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Jaclyn Trop",
        title: "3 indicators to watch for on GM Q2 earnings day",
        description: "The auto industry’s woes are far from over, but the second half of the year represents a return to planning for the future rather than responding to short-term supply chain crises. General Motors, which reports its second-quarter financial results Wednesday, …",
        url: "https://techcrunch.com/2022/07/25/3-indicators-to-watch-for-on-gm-q2-earnings-day/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/Blazer-EV-logo.jpeg?w=629",
        publishedAt: "2022-07-26T06:11:41Z",
        content: "The auto industrys woes are far from over, but the second half of the year represents a return to planning for the future rather than responding to short-term supply chain crises.\r\nGeneral Motors, wh… [+3106 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Catherine Shu",
        title: "Insightly helps engineering teams increase productivity and reduce burnout",
        description: "Insightly Analytics helps engineering teams stop problems before they happen, like slow release cycles, bottlenecks and uneven workload distribution that can lead to employee burnout. The San Francisco and Hyderabad-based startup announced today it has raised…",
        url: "https://techcrunch.com/2022/07/25/insightly-helps-engineering-teams-increase-productivity-and-reduce-burnout/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/Team-pic.jpg?w=600",
        publishedAt: "2022-07-26T06:00:56Z",
        content: "Insightly Analytics helps engineering teams stop problems before they happen, like slow release cycles, bottlenecks and uneven workload distribution that can lead to employee burnout. The San Francis… [+4614 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Rita Liao",
        title: "China's Jaka Robotics fueled by Saudi Arabia's Prosperity7 in global push",
        description: "Jaka Robotics, a Chinese startup that makes collaborative robots, has just pulled in a hefty Series D funding round of over $150 million from a lineup of heavyweight investors to help it expand globally. The round is led by Singapore’s sovereign wealth fund T…",
        url: "https://techcrunch.com/2022/07/25/softbank-temasek-saudi-aramco-china-jaka-robotitcs/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/Screenshot-2022-07-26-at-11.32.56-AM.png?w=764",
        publishedAt: "2022-07-26T03:37:17Z",
        content: "Jaka Robotics, a Chinese startup that makes collaborative robots, has just pulled in a hefty Series D funding round of over $150 million from a lineup of heavyweight investors to help it expand globa… [+3632 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Haje Jan Kamps",
        title: "Daily Crunch: Cartona will use $12M Series A to expand its Egypt-based, B2B e-commerce platform",
        description: "Happy new week! Christine went on a well-deserved break, so you’ll have to deal with a double dose of my awful puns and worse headline shenanigans for a bit.",
        url: "https://techcrunch.com/2022/07/25/daily-crunch-cartona-will-use-12m-series-a-to-expand-its-egypt-based-b2b-e-commerce-platform/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/558B7C00-37D7-4195-857C-8A0B121F179A.jpeg?w=600",
        publishedAt: "2022-07-25T22:40:10Z",
        content: "To get a roundup of TechCrunchs biggest and most important stories delivered to your inbox every day at 3 p.m. PDT,subscribe here.\r\nHappy new week! Christine went on a well-deserved break, so youll h… [+6404 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Rebecca Bellan",
        title: "GM joint venture to receive $2.5B government loan for U.S. battery plants",
        description: "The U.S. Department of Energy is reviving an old loan program and its first recipient is the joint battery venture between General Motors and LG Energy Solution. The $2.5 billion loan issued to GM and LG Energy will be used to help finance the construction of…",
        url: "https://techcrunch.com/2022/07/25/gm-joint-venture-to-receive-2-5b-government-loan-for-u-s-battery-plants/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/GettyImages-1229209981.jpg?w=711",
        publishedAt: "2022-07-25T22:25:16Z",
        content: "The U.S. Department of Energy is reviving an old loan program and its first recipient is the joint battery venture between General Motors and LG Energy Solution. The $2.5 billion loan issued to GM an… [+2950 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Kirsten Korosec",
        title: "Faraday's future on shaky ground as EV production delayed again",
        description: "Faraday Future has delayed production of its FF91 flagship electric vehicle due to lack of money and supply chain issues, the company said in a regulatory filing. The troubled EV startup-turned-publicly-traded-company said start of production and first delive…",
        url: "https://techcrunch.com/2022/07/25/faradays-future-on-shaky-ground-as-ev-production-delayed-again/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/03/faraday-facilities-002.jpg?w=600",
        publishedAt: "2022-07-25T21:31:15Z",
        content: "Faraday Future has delayed production of its FF91 flagship electric vehicle due to lack of money and supply chain issues, the company said in a regulatory filing.\r\nThe troubled EV startup-turned-publ… [+1681 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Devin Coldewey",
        title: "Instagram gets worse with dark patterns lifted from Tiktok",
        description: "I, of all people, got a fresh new user interface in Instagram the other day. Although the company has not rolled it out to all users yet, the changes seem in line with its intention to move away from its original model of photo sharing among friends, to the o…",
        url: "https://techcrunch.com/2022/07/25/instagram-gets-worse-with-dark-patterns-lifted-from-tiktok/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2020/09/instagram-glitch5.jpg?w=712",
        publishedAt: "2022-07-25T21:15:41Z",
        content: "I, of all people, got a fresh new user interface in Instagram the other day. Although the company has not rolled it out to all users yet, the changes seem in line with its intention to move away from… [+6849 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Amanda Silberling",
        title: "Peech is a text-to-speech app that actually sounds good",
        description: "Though it’s no replacement for quality human narration in an audiobook, Peech makes it easy to grab long documents and listen to them.",
        url: "https://techcrunch.com/2022/07/25/peech-text-to-speech-app/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/peech-app-image.png?w=711",
        publishedAt: "2022-07-25T20:56:55Z",
        content: "When you go into the App Store to download the Peech app, you’ll notice right away that the app’s icon displays the Ukrainian flag along with the company’s simple logo. But this is more than just a s… [+5214 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Dominic-Madori Davis, Rebecca Szkutak",
        title: "VCs and founders alike say investors should fold reproductive rights into ESG standards",
        description: "In the post-Roe era, some VC investors and founders believe the social definition of ESG investing should expand to incorporate pressing human rights issues, such as reproductive healthcare.",
        url: "https://techcrunch.com/2022/07/25/vcs-and-founders-alike-say-investors-should-fold-reproductive-rights-into-esg-standards/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/Yellow-City-Red-World.jpg?w=711",
        publishedAt: "2022-07-25T20:00:42Z",
        content: "Traditionally, U.S. private market investors who adopted environmental, social, and corporate governance (ESG) frameworks the very few thus far did so to evaluate a potential company or investment on… [+1031 chars]"
    },
    {
        source: {
            id: "techcrunch",
            name: "TechCrunch"
        },
        author: "Brian Heater",
        title: "Dead spiders: Nature’s robot hands",
        description: "This wasn’t a story you were expecting to read today, nor was it one I was expecting to write. Heck, judging from their interviews on the subject, it’s probably fair to say the team of mechanical engineers at Rice University weren’t expecting for their work t…",
        url: "https://techcrunch.com/2022/07/25/dead-spiders-natures-robot-hands/",
        urlToImage: "https://techcrunch.com/wp-content/uploads/2022/07/0718_NECRO-1-RN.jpeg?w=764",
        publishedAt: "2022-07-25T19:48:47Z",
        content: "This wasnt a story you were expecting to read today, nor was it one I was expecting to write. Heck, judging from their interviews on the subject, its probably fair to say the team of mechanical engin… [+1520 chars]"
    }
];

const length = _items.length;
_items.push(..._items);

const sleep = (ms = 0) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};

const createItem = (position, idx) => {
    const item = {
        styles: {
            transform: `translateX(${position * slideWidth}rem)`,
        },
        player: _items[idx],
    };

    switch (position) {
        case length - 1:
        case length + 1:
            item.styles = { ...item.styles };
            break;
        case length:
            break;
        default:
            item.styles = { ...item.styles };
            break;
    }

    return item;
};

const CarouselSlideItem = ({ pos, idx, activeIdx }) => {
    const item = createItem(pos, idx);

    return (
        <li className="carousel__slide_item" style={item.styles}>
            <div className="carousel__slide_item_img_link">
                <img src={item.player.urlToImage} alt={item.player.title} />
            </div>
            <div className="carousel_slide_item__body" >
                <h5>{item.player.title}</h5>
                <p>{item.player.description}</p>
            </div>
        </li>
    );
};

const keys = Array.from(Array(_items.length).keys());

export default function SlideShown() {

    const [items, setItems] = React.useState(keys);
    const [isTicking, setIsTicking] = React.useState(false);
    const [activeIdx, setActiveIdx] = React.useState(0);
    const bigLength = items.length;

    const prevClick = (jump = 1) => {
        if (!isTicking) {
            setIsTicking(true);
            setItems((prev) => {
                return prev.map((_, i) => prev[(i + jump) % bigLength]);
            });
        }
    };

    const nextClick = (jump = 1) => {
        if (!isTicking) {
            setIsTicking(true);
            setItems((prev) => {
                return prev.map(
                    (_, i) => prev[(i - jump + bigLength) % bigLength],
                );
            });
        }
    };

    const handleDotClick = (idx) => {
        if (idx < activeIdx) prevClick(activeIdx - idx);
        if (idx > activeIdx) nextClick(idx - activeIdx);
    };

    React.useEffect(() => {
        if (isTicking) sleep(300).then(() => setIsTicking(false));
    }, [isTicking]);

    React.useEffect(() => {
        setActiveIdx((length - (items[0] % length)) % length) // prettier-ignore
    }, [items]);

    return (
        <Box className="newsBox">
            <div className="newsBox_container">
                <div className="carousel__wrap">
                    <div className="carousel__inner">
                        <button className="carousel__btn carousel__btn__prev" onClick={() => prevClick()}>
                            <i className="carousel__btn_arrow carousel__btn_arrow__left" />
                        </button>
                        <div className="carousel__container">
                            <ul className="carousel__slide_list">
                                {items.map((pos, i) => (
                                    <CarouselSlideItem
                                        key={i}
                                        idx={i}
                                        pos={pos}
                                        activeIdx={activeIdx}
                                    />
                                ))}
                            </ul>
                        </div>
                        <button className="carousel__btn carousel__btn__next" onClick={() => nextClick()}>
                            <i className="carousel__btn_arrow carousel__btn_arrow__right" />
                        </button>
                    </div>
                </div>
            </div>
        </Box>
    );
};
