import { Avatar, Box, Card, CardContent, Grid, LinearProgress, Typography } from "@mui/material";
import InsertChartIcon from "@mui/icons-material/InsertChartOutlined";
import PeopleIcon from "@mui/icons-material/PeopleOutlined";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";

export default function Hard(setHardPopularQuestion, hardPopularQuestion) {
    let hardQuestionTitle = setHardPopularQuestion.hardPopularQuestion.title
    return (
        <Card>
            <CardContent>
                <Grid
                    container
                    spacing={3}
                    sx={{ justifyContent: 'space-between' }}
                >
                    <Grid item>
                        <Typography
                            color="textSecondary"
                            gutterBottom
                            variant="overline"
                        >
                            Top Hard of Question
                        </Typography>
                        <Typography
                            color="textPrimary"
                            variant="subtitle1"
                        >
                            {hardQuestionTitle}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Avatar
                            sx={{
                                backgroundColor: 'success.main',
                                height: 56,
                                width: 56
                            }}
                        >
                            <PeopleIcon className="iconMargin"/>
                        </Avatar>
                    </Grid>
                </Grid>
                <Box
                    sx={{
                        alignItems: 'center',
                        display: 'flex',
                        pt: 2
                    }}
                >
                    <ArrowUpwardIcon color="success" />
                    <Typography
                        variant="body2"
                        sx={{
                            mr: 1
                        }}
                    >
                        16%
                    </Typography>
                    <Typography
                        color="textSecondary"
                        variant="caption"
                    >
                        Since last month
                    </Typography>
                </Box>
            </CardContent>
        </Card>
    )
}