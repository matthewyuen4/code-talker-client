import { Avatar, Box, Card, CardContent, Grid, Typography } from '@mui/material';
import StarIcon from '@mui/icons-material/Star';
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import "../../assets/scss/iconD.scss"

export default function TotalProcess(setGeneralPopularQuestion, generalPopularQuestion) {
    let generalQuestionTitle = setGeneralPopularQuestion.generalPopularQuestion.title
    return (
        <Card>
            <CardContent>
                <Grid
                    container
                    spacing={3}
                    sx={{ justifyContent: 'space-between' }}
                >
                    <Grid item>
                        <Typography
                            color="textSecondary"
                            gutterBottom
                            variant="overline"
                        >
                            Top Of The Question
                        </Typography>
                        <Typography
                            color="textPrimary"
                            variant="subtitle1"
                        >
                            {generalQuestionTitle}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Avatar
                            sx={{
                                backgroundColor: 'warning.light',
                                height: 56,
                                width: 56,
                            }}
                        >
                            <StarIcon className="iconMargin"/>
                        </Avatar>
                    </Grid>
                </Grid>
                <Box
                    sx={{
                        alignItems: 'center',
                        display: 'flex',
                        pt: 2
                    }}
                >
                    <ArrowUpwardIcon color="success" />
                    <Typography
                        variant="body2"
                        sx={{
                            mr: 1
                        }}
                    >
                        16%
                    </Typography>
                    <Typography
                        color="textSecondary"
                        variant="caption"
                    >
                        Since last month
                    </Typography>
                </Box>
            </CardContent>
        </Card>
    )
}