import {Box, Grid} from "@mui/material";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import TotalProcess from "./grid-card/Total-process";
import Easy from "./grid-card/Easy";
import Hard from "./grid-card/Hard";
import Medium from "./grid-card/Medium";
import QuestionDataGrid from "./Question-Grid";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {getQuestionPopulation} from "../redux/questions/thunks";
import SlideShown from "./grid-card/News";

const Item = styled(Paper)(({theme}) => ({
    backgroundColor: '#0000003b',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));


export default function Dashboard() {
    const dispatch = useDispatch()

    const [generalPopularQuestion, setGeneralPopularQuestion] = useState([])
    const [easyPopularQuestion, setEasyPopularQuestion] = useState([])
    const [mediumPopularQuestion, setMediumPopularQuestion] = useState([])
    const [hardPopularQuestion, setHardPopularQuestion] = useState([])

    // console.log({ easyPopularQuestion })
    // console.log({ mediumPopularQuestion })
    // console.log({ hardPopularQuestion })
    const popularQuestions = async () => {
        let res = await dispatch(getQuestionPopulation()).unwrap()
        setGeneralPopularQuestion(res.generalPopularQuestion)
        setEasyPopularQuestion(res.easyPopularQuestion)
        setMediumPopularQuestion(res.mediumPopularQuestion)
        setHardPopularQuestion(res.hardPopularQuestion)
    }

    useEffect(() => {
        popularQuestions()
    }, [])

    return (
        <>
            <Box sx={{flexGrow: 1, py: 4, px: 4}}>
                <Grid container spacing={3} justifyContent="space-between" alignItems="center" width="100%">
                    <Grid xl={3} lg={6} sm={6} xs={12} padding="0.5rem">
                        <TotalProcess setGeneralPopularQuestion={setGeneralPopularQuestion}
                                      generalPopularQuestion={generalPopularQuestion}/>
                    </Grid>
                    <Grid xl={3} lg={6} sm={6} xs={12} padding="0.5rem">
                        <Easy setEasyPopularQuestion={setEasyPopularQuestion}
                              easyPopularQuestion={easyPopularQuestion}/>
                    </Grid>
                    <Grid xl={3} lg={6} sm={6} xs={12} padding="0.5rem">
                        <Medium setMediumPopularQuestion={setMediumPopularQuestion}
                                mediumPopularQuestion={mediumPopularQuestion}/>
                    </Grid>
                    <Grid xl={3} lg={6} sm={6} xs={12} padding="0.5rem">
                        <Hard setHardPopularQuestion={setHardPopularQuestion}
                              hardPopularQuestion={hardPopularQuestion}/>
                    </Grid>
                    <Grid item xs={12}>
                        <Item>
                            <SlideShown/>
                        </Item>
                    </Grid>
                    <Grid item xs={12}>
                        <QuestionDataGrid/>
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}