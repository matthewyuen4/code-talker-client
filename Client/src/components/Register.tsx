import {Outlet, useNavigate} from "react-router-dom";
import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {useForm} from "react-hook-form";
import {userRegister} from "../redux/auth/thunks";
import {Box, Button, Stack} from "@mui/material";
import '../assets/scss/register.scss'

interface registerForm {
    username: string;
    password: string;
    email: string;
    files: any;
}

export default function Register() {

    const [registerMessage, setRegisterMessage] = useState('')
    const [profileImage, setProfileImage] = useState<any>(null)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm<registerForm>({
        defaultValues: {
            username: "fung",
            password: "123456",
            email: "codetalker237@gmail.com",
            files: 'avatar'
        }
    });
    const onSubmit = async (data) => {
        console.log('onSubmit data:', data)
        try {
            console.log('data = ', data)
            const formData = new FormData()
            formData.append("username", data.username)
            formData.append("password", data.password)
            formData.append("email", data.email)
            formData.append("profile", data.files[0])
            await dispatch(userRegister(formData)).unwrap()
            navigate('/email-verification')
        } catch (e) {
            console.log('Register failed', e)
            setRegisterMessage(e)
        }
    };

    console.log(errors);

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h1 className="login-form">Register</h1>
                <label className="label" htmlFor="username">User Name</label>
                <input className="input" {...register("username", {required: true})} />

                <label className="label" htmlFor="password">Password</label>
                <input className="input" {...register("password", {required: true})} />

                <label className="label" htmlFor="email">Email</label>
                <input className="input" type="text" {...register("email", {required: true, pattern: /^\S+@\S+$/i})} />

                <div style={{color: "red"}}>
                    {Object.keys(errors).length > 0 &&
                        "Invalid Input, Please check your Username and Password and Upload Image."}
                    {registerMessage}
                </div>
                <Box>
                    <Stack direction="row" alignItems="center" spacing={2}>
                        <input accept="image/*" multiple type="file" {...register("files", {required: true})}/>
                    </Stack>
                </Box>
                <Box className="btnRegister">
                    <Button variant="contained" size="large" type="submit">
                        Register
                    </Button>
                </Box>
            </form>

            <Outlet/>
        </>
    );
}