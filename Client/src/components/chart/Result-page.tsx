import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {TrafficByDevice} from "./Chart";
import '../../assets/scss/grid-style.scss'
import Typography from "@mui/material/Typography";
import Confetti from "react-confetti";

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: '#ffffff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function ResultPage() {
    return (
        <Box sx={{ flexGrow: 1, py:4, px:4}}>
            <Confetti
                recycle={false}
                numberOfPieces={600}
            />
            <Typography variant="h3" gutterBottom component="div" className="gridTitle">
                Congratulations
            </Typography> <br/>
            <Typography variant="h4" gutterBottom component="div" className="gridTitle">
                You Have Finished The Question
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <Item>
                        hi

                    </Item>
                </Grid>
                <Grid item xs={4}>
                    <Item>
                        <TrafficByDevice />
                    </Item>
                </Grid>
            </Grid>
        </Box>
    );
}
