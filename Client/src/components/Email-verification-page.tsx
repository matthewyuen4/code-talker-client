import "../assets/scss/email-verification-page.scss";

export default function EmailVerification() {

    return (
        <>
            <div className="verificationMain">

                <h1>Thanks for your registration, please check your inbox!</h1>

                <div className="contentArea">
                    <div className="content">
                        <p>We just send you an e-mail request to confirm your registration. If you did not receive it please check your spam folder.</p>

                        <p>Do contact us when you cant find the confirmation mail.</p>

                        <p>Please click the link provided in the email to finalise your registration.</p>

                        <p>Regards.</p>

                        <p>Codie</p>
                    </div>
                </div>
            </div>
        </>
    );
}