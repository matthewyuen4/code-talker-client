import React, {useContext, useEffect, useRef, useState} from "react";
import CodeMirror from "@uiw/react-codemirror";
import {javascript} from "@codemirror/lang-javascript";
import {sublime} from "@uiw/codemirror-theme-sublime";
import {SocketStore} from "../contexts/socketContext";
import ReactMarkdown from "react-markdown";
import {useNavigate, useParams} from "react-router-dom";
import {Avatar, Box, Button, Grid, Modal, Stack, TextField} from "@mui/material";
import Typography from "@mui/material/Typography";
import PlayCircleFilledIcon from "@mui/icons-material/PlayCircleFilled";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import RefreshIcon from '@mui/icons-material/Refresh';
import CheckIcon from '@mui/icons-material/Check';
import "../assets/scss/code-Editor.scss";
import Confetti from "react-confetti";
import FileCopyOutlinedIcon from '@mui/icons-material/FileCopyOutlined';
import FileCopyIcon from '@mui/icons-material/FileCopy';

const Item = styled(Paper)(({theme}) => ({
    backgroundColor: "#303841",
    ...theme.typography.body2,
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
}));

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 2,
};

export const CodeEditor = () => {
    let {
        content,
        handleEditorChange,
        setContent,
        roomMeta,
        sk,
        setAnswer,
        answer,
        getRoomInfoFromDB,
        mdQ,
        qTitle,
        qId,
        userId,
        player,
        submitStatus
    } = useContext(SocketStore);
    const codeMirrorRef = useRef<any>();
    const mdValue = mdQ;
    const {roomId} = useParams();
    const [isClicked, setIsClicked] = useState(false)
    const navigate = useNavigate()
    const handleClose = () => navigate('/home');

    const displayAns = answer.map((line: string) => (
        <div className="">{line.replace("√", "✅").replace("×", "❌")}</div>
    ));

    const execute = () => {
        sk.emit("execute", {roomId: roomMeta.roomId, question_id: qId, language: "ts"});
        setAnswer(["execution in progress, please wait . . . "]);
    };
    const submit = () => {
        console.log("submit button clicked");
        sk.emit("submit", {
            code: codeMirrorRef.current.view.contentDOM.innerText.trim(),
            roomId: roomMeta.roomId,
            question_id: qId,
            userId: userId,
        });
        setAnswer(["submission in progress, please wait . . . "]);

    };


    useEffect(() => {
        try {
            getRoomInfoFromDB();
        } catch (e) {
            console.error(e);
        }
    }, [getRoomInfoFromDB]);

    return (
        <Box sx={{flexGrow: 0, py: 1, px: 4}}>
            <>
                {submitStatus ? (
                    <>
                        <Confetti
                            recycle={false}
                            numberOfPieces={600}
                        /> &&
                        <Modal
                            open={true}
                            onClose={handleClose}
                            aria-labelledby="modal-modal-title"
                            aria-describedby="modal-modal-description"
                        >
                            <Box sx={style}>
                                <Typography id="modal-modal-title" variant="h6" component="h2">
                                    Complete
                                </Typography>
                                <Typography id="modal-modal-description" variant="body1" sx={{ mt: 2 }}>
                                    {displayAns}
                                </Typography>
                            </Box>
                        </Modal>
                    </>
                ) : (<></>)}
            </>
            <Grid container direction="row" spacing={1} justifyContent="space-evenly" max-height="50vh">
                <Grid item xs={5} padding="0.1rem" width="100%" maxHeight="10vh">
                    <Grid item xs={5} padding="0.5rem">
                        <Typography variant="h5" gutterBottom component="div" className="EditorRoomIDTitle">
                            Your room ID:<div onClick={() => {
                                navigator.clipboard.writeText(roomId); setIsClicked(true)
                            }}>
                                {roomId}{isClicked ? <FileCopyIcon /> : <FileCopyOutlinedIcon />}
                            </div>
                        </Typography>
                        <Typography variant="h5" gutterBottom component="div" className="EditorRoomIDTitle">
                            Question Title: {qTitle}
                        </Typography>
                    </Grid>
                    <Grid item xs={5} padding="0rem">
                        <Stack direction="row" className="EditorAvatarStyle">
                            {player.map((avatar) => (
                                <Avatar src={`http://localhost:8080/${avatar}`} alt="user"/>
                            ))}
                        </Stack>
                    </Grid>
                    <ReactMarkdown className="ReactMarkdown" children={mdValue}/>

                    <Item className="ExecutionAreaStyle">{displayAns}</Item>

                    <Grid item className="EditorBtn">
                        <Button
                            variant="contained"
                            className="EditorBtnStyle"
                            startIcon={<PlayCircleFilledIcon/>}
                            onClick={execute}
                        >
                            Execute
                        </Button>
                        <Button
                            variant="contained"
                            className="EditorBtnStyle"
                            startIcon={<CheckIcon/>}
                            onClick={submit}
                        >
                            Submit
                        </Button>
                        <Button
                            variant="contained"
                            className="EditorBtnStyle"
                            startIcon={<RefreshIcon/>}
                            onClick={() => {
                                setContent("console.log('hello world!')");
                            }}
                        >
                            Restart
                        </Button>
                    </Grid>
                </Grid>

                <Grid item xs={7} padding="0.5rem">
                    <CodeMirror
                        className="EditorInput"
                        ref={codeMirrorRef}
                        theme={sublime}
                        value={content}
                        height="86vh"
                        width="50vw"
                        extensions={[javascript({jsx: false, typescript: true})]}
                        onKeyUp={(e) => {
                            handleEditorChange(codeMirrorRef.current.view.contentDOM.innerText.trim());
                        }}
                    />
                </Grid>
            </Grid>
        </Box>
    );
};
