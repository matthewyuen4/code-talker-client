import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { deleteQuestionToFavorites, saveQuestionToFavorites } from "../redux/questions/thunks";
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import "../assets/scss/favoriteStyle.scss"
import useQuestionDetails from "../hook/useQuestionDetails";
import { createRoomAndJoinRoomViaURL } from "../redux/room/thunks";
import { useSelect } from '@mui/base';
import { stat } from 'fs';
import { IconButton } from '@mui/material';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send'

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


export default function QuestionInfoModal({ selectedQuestion: selectedQuestion, setSelectedQuestion }) {

    const [invitationMessage, setInvitationMessage] = useState('')
    const [sentInvitationEmail, setSentInvitationEmail] = useState(false)
    const [isClicked, setIsClicked] = useState(false)
    const [emailLIst, setEmailList] = useState([])
    const [selectedQuestionId, setSelectedQuestionId] = useState('')
    const handleClose = () => { setSelectedQuestion(null); setIsClicked(false) };
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const { roomId } = useParams()

    const { questionDetails, fetchQuestionDetails, setQuestionDetails } = useQuestionDetails()


    const navigateToCodeMirror = async () => {
        let res = await dispatch(createRoomAndJoinRoomViaURL({
            emailList: emailLIst,
            questionDetails
        })).unwrap()
        console.log("emailLIst", emailLIst)
        console.log(res.username)
        let roomId = res.roomId
        navigate('/code/' + roomId)
    }

    const favThus = useSelector((state: any) => state.userData.userData.favorites)

    const favStatus = () => {
        if (favThus !== null) {
            const favList = favThus.map(x => x.id)
            if (selectedQuestion !== null) {
                let selectedQuestionId = selectedQuestion.id
                console.log({ selectedQuestionId })
                // setSelectedQuestionId(selectedQuestion)
                let result = favList.indexOf(selectedQuestionId)
                console.log({ result })
                if (result === -1) {
                    setIsClicked(false)
                } else if (result === 0) {
                    setIsClicked(true)
                }
            }
        }

    }



    const fetchUserFavoriteList = async () => {
        if (isClicked === false) {
            setIsClicked(true)
            await dispatch(saveQuestionToFavorites({
                questionDetails
            })).unwrap()
        } if (isClicked == true) {
            setIsClicked(false)
            await dispatch(deleteQuestionToFavorites({
                questionDetails
            })).unwrap()
        }
        // console.log("f", questionDetails.id)
        // await dispatch(saveQuestionToFavorites({
        //     questionDetails
        // })).unwrap()
        // console.log("3")
        // if (questionDetails.is_fav === false) {
        //     setIsClicked(false)
        // } else {
        //     setIsClicked(true)
        // }
    }

    useEffect(() => {
        // setIsClicked(false)
        console.log('selectedQuestion changed :', selectedQuestion)
        let main = async () => {
            await fetchQuestionDetails(selectedQuestion.id)
        }
        if (selectedQuestion && selectedQuestion.id && questionDetails === null) {
            console.log('called main :', main)
            main()
        }
        if (selectedQuestion === null) {
            setQuestionDetails(null)
            console.log("good", questionDetails)
        }
        favStatus()
        // if(questionDetails === true){
        //     setIsClicked(true)
        // }else{
        //     setIsClicked(false)
        // }
        // return ()=>{
        //     setQuestionDetails({})
        // }
    }, [selectedQuestion])


    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: '#263238',
        color: '#fff',
        border: '2px solid #000',
        boxShadow: 24,
        pt: 2,
        px: 4,
        pb: 3,
    };

    return (
        <div>
            {selectedQuestion && Object.keys(selectedQuestion).length > 0 && questionDetails &&
                <Modal
                    open={selectedQuestion && Object.keys(selectedQuestion).length > 0}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >

                    <Box sx={{ ...style, width: 600, height: 350, overflow: "auto" }}>
                        <Typography id="modal-modal-title" variant="h4" component="h1" className="bookTag">
                            {questionDetails.title}
                            <Box onClick={() => fetchUserFavoriteList()}>
                                {isClicked ? <BookmarkIcon /> : <BookmarkBorderIcon />}
                            </Box>
                        </Typography>
                        <Typography id="modal-modal-description" className="grid-detail" sx={{ mt: 2, mb: 2 }}>
                            {questionDetails.question}
                        </Typography>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Invite Your Friend
                            <IconButton onClick={() => {
                                setEmailList((oldEmailList) => {
                                    return [...oldEmailList, ""]
                                })
                            }
                            }
                                color="primary" aria-label="upload picture" component="label">
                                <AddCircleOutlineIcon />
                            </IconButton>
                        </Typography>

                        {emailLIst && emailLIst.length > 0 && emailLIst.map((email, index) => {
                            return (
                                <div key={index}>
                                    <input type="email" style={{ minWidth: "200px" }} value={email} onChange={(e) => {
                                        setEmailList((t) => {
                                            t[index] = e.target.value
                                            return [...t]
                                        })
                                    }}></input>
                                    <IconButton onClick={() => {
                                        setEmailList((oldEmailList) => {
                                            oldEmailList.splice(index, 1)
                                            return [...oldEmailList]
                                        })
                                    }}
                                        color="primary" aria-label="upload picture" component="label">
                                        <DeleteIcon color='error' />
                                    </IconButton>
                                </div>
                            )
                        })
                        }
                        <Button onClick={navigateToCodeMirror} color='success' variant="contained" endIcon={<SendIcon />}>
                            Start Now
                        </Button>
                    </Box>
                </Modal>}
        </div>
    );
}
