import React, {useState} from "react";
import {useForm} from "react-hook-form";
import "../assets/scss/login.scss";
import {Outlet, useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {login} from "../redux/auth/thunks";
import {styled} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import {Alert, AlertTitle, Box, Button} from "@mui/material";

interface LoginForm {
    email: string;
    password: string;
}

const Item = styled(Paper)(({theme}) => ({
    backgroundColor: '#ffffff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function LoginPage() {

    const [loginMessage, setLoginMessage] = useState('')
    const [havError, setHavError] = useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm<LoginForm>({});
    const onSubmit = async (data) => {
        try {
            await dispatch(login({...data})).unwrap()
            setTimeout(() =>
                navigate('/home')
                , 5000)
            setHavError(true)
        } catch (e) {
            console.log('login fail : ', e)
            setTimeout(() =>
                    setLoginMessage(e)
                , 5000)
        }
    };

    console.log(errors);

    const registerPage = () => {
        setTimeout(() =>
                navigate('/register')
            , 2000)
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h1 className="login-form">Login</h1>
                <label className="label" htmlFor="email">Email</label>
                <input className="input"  {...register("email", {required: true})} />

                <label className="label" htmlFor="Password">Password</label>
                <input className="input" {...register("password", {required: true})} />

                <div style={{color: "red"}}>
                    {Object.keys(errors).length > 0 &&
                        "Invalid Input, Please check your Username and Password."}
                    {loginMessage}
                    {havError ? (<Alert severity="success">
                        <AlertTitle>Success</AlertTitle>
                        You have <strong>Success to Login!</strong>
                    </Alert>): (<></>)}
                </div>
                <Box className="btnStyle">
                    <Button variant="contained" size="large" type="submit">
                            Login
                        </Button>
                    <Button variant="contained" size="large" onClick={registerPage}>
                        Register
                    </Button>
                </Box>
            </form>
            <Outlet/>
        </>
    );
}