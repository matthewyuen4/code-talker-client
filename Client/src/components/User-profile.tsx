import "../assets/scss/user_profile.scss";
import React, { useCallback, useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import { Avatar, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { fetchUserData } from "../redux/UserData/thunks";
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import QuestionInfoModal from "./QuestionInfoModal";
// const historyDummy = [
//   {
//     "question_id": 2,
//     "answer": "/**\r\n * @param {number[]} nums\r\n * @param {number} target\r\n * @return {number[]}\r\n */\r\nvar twoSum = function(nums, target) {\r\nlet result = []\r\n\r\nfor(let j = 0; j <nums.length ; j++){\r\n    for(let i = 0; i < nums.length ; i++){\r\n    if(i == j){\r\n        continue\r\n    } else {\r\n        if(nums[j] + nums[i] == target){\r\n            result.push(i,j)\r\n            return result\r\n        }\r\n    }\r\n}\r\n}\r\n    \r\n};",
//     "title": "Add Two Numbers"
//   },
//   {
//     "question_id": 3,
//     "answer": "/**\r\n * @param {number[]} nums\r\n * @param {number} target\r\n * @return {number[]}\r\n */\r\nvar twoSum = function(nums, target) {\r\nlet result = []\r\n\r\nfor(let j = 0; j <nums.length ; j++){\r\n    for(let i = 0; i < nums.length ; i++){\r\n    if(i == j){\r\n        continue\r\n    } else {\r\n        if(nums[j] + nums[i] == target){\r\n            result.push(i,j)\r\n            return result\r\n        }\r\n    }\r\n}\r\n}\r\n    \r\n};",
//     "title": "Minus Two Numbers"
//   }
// ]
export default function UserProfile() {
  const [value, setValue] = React.useState(0);
  const [selectedQuestion, setSelectedQuestion] = React.useState(null)

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
  }

  const [expandedHistory, setExpandedHistory] = React.useState<number[]>([]);

  const handleClick = (id: number) => {

    let idPosition = expandedHistory.indexOf(id)
    console.log('idPosition = ', idPosition)
    if (idPosition === -1) {
      console.log('add into  array')
      setExpandedHistory((prev) => {
        return [...prev, id]
      });
    } else {
      setExpandedHistory((prev) => {
        prev.splice(idPosition, 1)
        console.log('prev :', prev)
        return [...prev]
      });
    }

  };

  const dispatch = useDispatch();
  const { userInfo, favorites, history } = useSelector((state: any) => state.userData.userData)
  console.log({ history })
  useEffect(() => {
    dispatch(fetchUserData()).unwrap()
  }, [])

  function Favorite(favorite: any, index: number) {
    return (
      <>
        <div onClick={() => {
          setSelectedQuestion(
            favorite
          )
        }} className="sub-area" key={index} id={favorite["id"]}>{favorite["title"]}
        </div>
      </>
    )
  }

  const isHistoryExpanded = useCallback((id: number) => {
    return expandedHistory.indexOf(id) === -1
  }, [expandedHistory])

  function History() {
    return (
      <div>
        <div className="sub-area">
          <span>Title</span><span>Submission time</span>
        </div>
        <div>
          <List sx={{ width: '80%' }}>
            {history.length > 0 && history.length && history.map(historyItem => {
              return (
                <span key={historyItem.question_id}>
                  <ListItemButton sx={{ borderBottom: "1px solid grey", color: 'wheat', fontWeight: "bold", paddingLeft: 0, paddingBottom: 0, paddingTop: "1px" }} onClick={() => {
                    handleClick(historyItem.question_id)
                  }}>
                    <ListItemText primary={historyItem["title"]} />
                    <p>{historyItem.created_at.substr(0, 10)} {historyItem.created_at.substr(-13, 8)} </p>
                    {isHistoryExpanded(historyItem.question_id) ? <ExpandLess /> : <ExpandMore />}
                  </ListItemButton>
                  <Collapse in={isHistoryExpanded(historyItem.question_id)} timeout="auto" unmountOnExit>
                    <List component="div" sx={{ paddingTop: "0.6rem" }} disablePadding>
                      <div> Complete Status: <button className={"button_" + historyItem.complete_status}>{historyItem.complete_status}</button> </div>
                      {/* <ListItemText primary={historyItem["answer"]} /> */}

                    </List>
                  </Collapse>
                </span>
              )
            })}
          </List>
        </div>
      </div>
    )
  }

  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`vertical-tabpanel-${index}`}
        aria-labelledby={`vertical-tab-${index}`}
        style={{ width: "80%" }}
        {...other}
      >
        {value === index && (
          <Box sx={{ paddingLeft: 3, paddingTop: 2, width: "inherit" }}>{/* padding */}
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  function a11yProps(index: number) {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  }

  return (
    <div className="card-area">
      <div className="card-container">
        <div className="header">
          <div className="profile-area">
            {/*<Avatar src={`http://localhost:8080/${userInfo[0]['avatar']}`} alt="user"/>*/}
            <img src={`http://localhost:8080/${userInfo[0]['avatar']}`} alt="user" />
          </div>
          <div className="details-area">
            <h1 >Welcome, <span>{userInfo[0]['username']}</span></h1>
          </div>
        </div>
        <div className="rows">
          <Box
            sx={{ flexGrow: 3, display: 'flex' }}
          >
            <Tabs
              orientation="vertical"
              value={value}
              onChange={handleChange}
              textColor="secondary"
              indicatorColor="secondary"
              sx={{ borderRight: 1, borderColor: 'divider' }}
            >
              <Tab sx={{ color: "white" }} label="Account Details" {...a11yProps(0)} />
              <Tab sx={{ color: "white" }} label="Favorite" {...a11yProps(1)} />
              <Tab sx={{ color: "white" }} label="History" {...a11yProps(2)} />
            </Tabs>
            <TabPanel value={value} index={0}>
              <header>User Information</header>
              <div className="sub-area">User Name </div>
              <div>{userInfo[0]['username']}</div>
              <div className="sub-area">Role</div>
              <div>{userInfo[0]['role']}</div>
              <div className="sub-area">Status</div>
              <div>{userInfo[0]['status']}</div>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <header>Favorites</header>
              <QuestionInfoModal setSelectedQuestion={setSelectedQuestion} selectedQuestion={selectedQuestion}></QuestionInfoModal>
              <div>{favorites.map((favorite, index) => Favorite(favorite, index))}</div>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <header>History</header>
              <History />
            </TabPanel>
          </Box>
        </div>
      </div>
    </div>
  )
}