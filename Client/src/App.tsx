import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import LoginPage from "./components/Login";
import UserProfile from "./components/User-profile";
import NotFoundError from "./components/Not-found-page";
import Navbar from "./layout/nav/navbar";
import SideNavbar from "./layout/side-nav/side-navbar";
import { SocketProvider } from "./contexts/socketContext";
import { CodeEditor } from "./components/Editor";
import RequireAuth from "./components/RequireAuth";
import Register from "./components/Register";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "./redux/auth/state";
import { loginWithToken } from "./redux/auth/thunks";
import "../src/assets/scss/index.scss";
import Box from "@mui/material/Box";
import BasicExampleDataGrid from "./components/Question-Grid";
import QuestionDataGrid from "./components/Question-Grid";
import ResultPage from "./components/chart/Result-page";
import Dashboard from "./components/Dashboard";
import EmailVerification from "./components/Email-verification-page";
import SlideShown from "./components/grid-card/News";


function App() {
	const dispatch = useDispatch();
	const isLoggedIn = useSelector((state: IRootState) => state.auth.isLoggedIn);

	useEffect(() => {
		async function main() {
			if (isLoggedIn === undefined) {
				await dispatch(
					loginWithToken({
						token: localStorage.getItem("token"),
					})
				);
			}
		}
		main();
	}, [isLoggedIn]);

	return (
		<Box className="dFlex">
			<BrowserRouter>
				<SideNavbar />
				<Box className="flexGrow">
					<nav>
						<Navbar />
					</nav>
					<Routes>
						<Route path="/questions" element={<QuestionDataGrid />}></Route>
						<Route path="/login" element={<LoginPage />}></Route>
						<Route path="/email-verification" element={<EmailVerification />}></Route>
						<Route path="/register" element={<Register />}></Route>
						<Route path="/result" element={<ResultPage />}></Route>`
						<Route path="/home" element={<Dashboard />}></Route>
						<Route path="/code" element={<SlideShown />}></Route>
						<Route path="/" element={<RequireAuth />}>
							<Route path="questions2" element={<ResultPage />}></Route>
							<Route path="chat" element={<BasicExampleDataGrid />}></Route>
							<Route path="user" element={<UserProfile />}></Route>
							<Route path="code" element={<SocketProvider><CodeEditor /></SocketProvider>}></Route>
						</Route>
						<Route path="code">
							<Route path=":roomId" element={<SocketProvider><CodeEditor /></SocketProvider>}></Route>
						</Route>
						<Route path="*" element={<NotFoundError />}></Route>
					</Routes>
				</Box>
			</BrowserRouter>
		</Box>
	);
}

export default App;
